#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Goto main directory
cd $DIR/main


# Initialise Kotlin/Native Project
$DIR/gradle/bin/gradle build --warning-mode=all