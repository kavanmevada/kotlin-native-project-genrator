#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Create Porject Dir
mkdir $DIR/main

# Goto main directory
cd $DIR/main


# Initialise Kotlin/Native Project
# $DIR/gradle/bin/gradle



# fullname="USER INPUT"
read -p "Project name (example: Demo):" PROGECT_NAME

until [[ $PROGECT_NAME =~ ^[A-Za-z_]+$ ]]; do
    echo "Only letters are allowed !"
    read -p "Project name (default: Demo):" PROGECT_NAME
done




# user="USER INPUT"
read -p "Source package (example: sample.helloworld):" SOURCE_PKG

until [[ $SOURCE_PKG =~ ^[a-z.]+$ ]]; do
    echo "Only lowercase letters are allowed !"
    read -p "Project name (default: Demo):" PROGECT_NAME
done





# Create build.gradle file
cat <<EOF > build.gradle
plugins {
    id 'org.jetbrains.kotlin.multiplatform' version '1.3.20'
}

repositories {
    mavenCentral()
}

kotlin {
    // For ARM, preset function should be changed to iosArm32() or iosArm64()
    // For Linux, preset function should be changed to e.g. linuxX64()
    // For MacOS, preset function should be changed to e.g. macosX64()
    macosX64('$PROGECT_NAME') {
        binaries {
            // Comment the next section to generate Kotlin/Native library (KLIB) instead of executable file:
            executable('${PROGECT_NAME}App') {
                // Change to specify fully qualified name of your application's entry point:
                entryPoint = '${SOURCE_PKG}.main'
            }
        }
    }
}

// Use the following Gradle tasks to run your application:
// :runHelloWorldAppReleaseExecutableHelloWorld - without debug symbols
// :runHelloWorldAppDebugExecutableHelloWorld - with debug symbols
EOF


# Create derectories
mkdir -p src/${PROGECT_NAME}Main/Kotlin

# Create Demo file
cat <<EOF > src/${PROGECT_NAME}Main/Kotlin/Main.kt
package ${SOURCE_PKG}

fun hello(): String = "Hello, Kotlin/Native!"

fun main() {
    println(hello())
}
EOF